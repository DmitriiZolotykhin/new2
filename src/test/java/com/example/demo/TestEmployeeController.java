package com.example.demo;
import com.example.demo.controller.EmployeeController;
import com.example.demo.mapper.Employee;
import com.example.demo.service.EmployeeService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import java.util.Collections;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.mockito.BDDMockito.doNothing;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@Import(TestConfig.class)
@WebMvcTest(EmployeeController.class)

public class  TestEmployeeController  {
        private static final int ID = 101;

        @Autowired
        private Employee employee;

        @Autowired
        private ObjectMapper objectMapper;

        @Autowired
        private MockMvc mock;

        @MockBean
        private EmployeeService service;

        @Test
        public void findAllEmployee() throws Exception {
            List<Employee> employees = Collections.singletonList(employee);
            given(service.findAll()).willReturn(employees);
            mock.perform(get("app/employees"))
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(content().json(objectMapper.writeValueAsString(employee)));
            verify(service).findAll();
        }

        @Test
        public void findById() throws Exception {
            given(service.findById(ID)).willReturn(employee);

            mock.perform(get("app/employees/id={id}", ID))
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(content().json(objectMapper.writeValueAsString(employee)));

            verify(service).findById(ID);
        }



        @Test
        public void createEmployee() throws Exception {
            doNothing().when(service).save(employee);
            mock.perform(post("app/employees")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(employee)))
                    .andDo(print())
                    .andExpect(status().isCreated());
            verify(service).save(employee);
        }

        @Test
        public void deleteEmployee() throws Exception {
            doNothing().when(service).deleteById(ID);
            mock.perform(delete("app/employees/id={id}", ID))
                    .andDo(print())
                    .andExpect(status().isNoContent());
            verify(service).deleteById(ID);
        }

        @Test
        public void updatePlayer() throws Exception {
            doNothing().when(service).save(employee);
            mock.perform(put("app/employees")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(employee)))
                    .andDo(print())
                    .andExpect(status().isOk());
            verify(service).save(employee);
        }
    }
