package com.example.demo;

import com.example.demo.mapper.Employee;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

@TestConfiguration
public class TestConfig {
    @Bean
    public Employee employeeBuilder() {
        return new TestEmployeeBuilder()
                .setFirstName("Dmitrii")
                .setLastName("Alexanderov")
                .setEmailId("qq@gmail.com")


                .build();
    }
}


