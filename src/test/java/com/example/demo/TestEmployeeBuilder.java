package com.example.demo;

import com.example.demo.mapper.Employee;

public class TestEmployeeBuilder {

    private String firstName;
    private String lastName;
    private String emailId;


    public Employee build() {
        Employee employee = new Employee();
        employee.setFirstName(firstName);
        employee.setLastName(lastName);
        employee.setEmailId(emailId);
        return employee;
    }



    public TestEmployeeBuilder setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public TestEmployeeBuilder setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public TestEmployeeBuilder setEmailId(String emailId) {
        this.emailId = emailId;
        return this;
    }

}

