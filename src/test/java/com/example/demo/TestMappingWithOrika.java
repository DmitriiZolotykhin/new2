package com.example.demo;

import com.example.demo.mapper.Employee;
import com.example.demo.mapper.EmployeeDto;
import com.example.demo.mapper.MapperConfiguration;
import ma.glasnost.orika.MapperFacade;
import org.junit.jupiter.api.Test;

//Test Для проверки Orika маппинга
//не разобрался как связать MapperEmployeeDtoToEmployee and MapperEmployeeToEmployeeDto
public class TestMappingWithOrika {
    @Test
    public void testMappingWithOrika() {
        MapperFacade facade = new MapperConfiguration();
        Employee e = new Employee();
        e.setId(404);
        e.setFirstName("DDD");
        e.setLastName("FF");
        e.setEmailId("tyuy");
        EmployeeDto dto = facade.map(e, EmployeeDto.class);
        System.out.println(dto);
        System.out.println(e);

    }
}
