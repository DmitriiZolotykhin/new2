package com.example.demo.dao;

import com.example.demo.mapper.Employee;


import java.util.List;

public interface EmployeeDao {

    List<Employee> findAll();

    Employee findById(int id);

    void save(Employee employee);

    void deleteById(int id);
}