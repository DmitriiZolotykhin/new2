package com.example.demo.dao;
import com.example.demo.mapper.Employee;
import lombok.NoArgsConstructor;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;



@Repository // помечает определенный класс как объект доступа к данным
public class EmployeeDaoImpl implements EmployeeDao {
    // определить поле для entityManager

    private EntityManager entityManager;

    @Autowired

    public EmployeeDaoImpl(EntityManager theEntityManager) {
        entityManager = theEntityManager;

    }

    @Override
    public List<Employee> findAll() {

        // получить текущий сеанс  hibernate session
        Session currentSession = entityManager.unwrap(Session.class);
        // создаем запрос
        // используя собственный Hibernate API
        Query<Employee> query = currentSession.createQuery("FROM Employee", Employee.class);
        // выполнить запрос и получить список результатов
        List<Employee> employee = query.getResultList();
        // вернуть результаты
        return employee;
    }

    @Override
    public Employee findById(int id) {

        int theId = id;
        Session currentSession = entityManager.unwrap(Session.class);
        Employee employee = currentSession.get(Employee.class, theId);
        return employee;
    }

  @Override
    public void save(Employee employee) {

        Session currentSession = entityManager.unwrap(Session.class);
        currentSession.saveOrUpdate(employee);
    }

    @Override
    public void deleteById(int id) {

        int theId = id;
        Session currentSession = entityManager.unwrap(Session.class);
        Query<Employee> query = currentSession.createQuery("DELETE FROM  Employee  WHERE id=:idToDelete");
        query.setParameter("idToDelete", id);
        query.executeUpdate();
    }
}