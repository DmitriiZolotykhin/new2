package com.example.demo.dao;

import com.example.demo.mapper.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Repository
//  определенный класс как объект доступа к данным
public class EmployeeDaoJpaImpl implements EmployeeDao {


    // определить поле для entityManager
    private EntityManager entityManager;

    @Autowired
    public EmployeeDaoJpaImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public List<Employee> findAll() {

        Query query = entityManager.createQuery("FROM Employee");
        List<Employee> employees = query.getResultList();
        return employees;
    }

    @Override
    public Employee findById(int id) {
        Employee employee = entityManager.find(Employee.class, id);
        return employee;
    }

    @Override
    public void save(Employee employee) {
        Employee employeeDB = entityManager.merge(employee);
        employee.setId(employeeDB.getId());
    }

    @Override
    public void deleteById(int id) {
        Query query = entityManager.createQuery("DELETE FROM Employee WHERE id=:idToDelete");
        query.setParameter("idToDelete", id);
        query.executeUpdate();
    }
}