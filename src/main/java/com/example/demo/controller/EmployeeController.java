package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;

import com.example.demo.mapper.EmployeeDto;
import com.example.demo.mapper.Employee;
import com.example.demo.mapper.MapperEmployeeDtoToEmployee;
import com.example.demo.mapper.MapperEmployeeToEmployeeDto;
import com.example.demo.service.EmployeeService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController

@RequestMapping("/app")
public class EmployeeController {

    //todo После наименования класса пробел ----вроде был пробел?+
    //todo Давай перейдем на lombok. При связывании сервисов ты используешь правильный ресурсоэкономный формат++
    //todo Но ломбок по сути корпоративный стандарт, чтобы в нем сделать то же надо сделать:++
    // поля сервисов сделай private final и на класс повесь аннотацию @RequiredArgsConstructor +++
    // такая конструкция прилично экономит место++
    //todo сделай это во всех сервисах++
    // todo добавь для контроллеров тестов+++
    // используй в них @RunWith(SpringRunner.class) @WebMvcTest(EmployeeController.class) и MockMvc#perform++ не протестил не запускаются тесты

    @NonNull private final EmployeeService employeeService;

    @NonNull private final MapperEmployeeDtoToEmployee employeeDtoToEmployee;
    @NonNull private final MapperEmployeeToEmployeeDto employeeToEmployeeDto;
/**

    @Autowired
    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @Autowired
    public void setEmployeeDtoToEmployee(MapperEmployeeDtoToEmployee employeeDtoToEmployee) {
        this.employeeDtoToEmployee = employeeDtoToEmployee;
    }

    @Autowired
    public void setEmployeeToEmployeeDto(MapperEmployeeToEmployeeDto employeeToEmployeeDto) {
        this.employeeToEmployeeDto = employeeToEmployeeDto;
    }
**/
    //expose "/employees" and return list of employees
    @GetMapping("/employees")
    public ResponseEntity<List<EmployeeDto>> findAll() {

        List<EmployeeDto> employeeDtos = new ArrayList<>();

        for (Employee employee : employeeService.findAll()) {
            employeeDtos.add(employeeToEmployeeDto.convert(employee));
        }

        return new ResponseEntity<>(employeeDtos, HttpStatus.OK);
        //return employeeService.findAll();
    }

    /**
     * @param id это employees id
     * @return ответ для entity
     * <p>
     * <p>
     * Класс ResponseEntity представляет HTTP-ответ, состоящий из заголовков,
     * тело и код ответа.
     * Используется для возврата определенного кода состояния http с контроллера.
     */
    @GetMapping("/employees/{id}")
    public ResponseEntity<EmployeeDto> getEmployee(@PathVariable Integer id) {

        Employee employee = employeeService.findById(id);
        if (employee == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(employeeToEmployeeDto.convert(employee), HttpStatus.OK);
    }

    //@PathVariable аннотацию можно использовать для аргумента метода контроллера, чтобы связать его со значением переменной шаблона URI:
    //@RequestBody может использоваться в контроллере для реализации сериализации и десериализации объектов
    @PostMapping("/employees")
    public ResponseEntity<EmployeeDto> addEmployee( @RequestBody EmployeeDto employeeDto) {

        if (employeeDto.getId() != null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        employeeService.save(employeeDtoToEmployee.convert(employeeDto));
        return new ResponseEntity<>(HttpStatus.CREATED);
    }


    @PutMapping("employees/{id}")
    public ResponseEntity<EmployeeDto> updateEmployee( @RequestBody EmployeeDto employeeDto, @PathVariable Integer id) {

        employeeDto.setId(id);
        employeeService.save( employeeDtoToEmployee.convert(employeeDto));
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @DeleteMapping("employees/{id}")
    public ResponseEntity<EmployeeDto> deleteEmployeeById(@PathVariable Integer id) {

        try {
            employeeService.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}



