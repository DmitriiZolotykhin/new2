package com.example.demo.mapper;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

// Переходим на lombok добавь просто одну аннатацию @Data и удале все  @Getter @Setter @ToString++


@Data

// Переходим на lombok добавь просто одну аннатацию @Data и удале все  @Getter @Setter @ToString

public class EmployeeDto {
    private  Integer id;
    private  String firstName;
    private  String lastName;
    private  String emailId;

/**
 public Integer getId() {
 return id;
 }

 public void setId(Integer id) {
 this.id = id;
 }

 public String getFirstName() {
 return firstName;
 }

 public void setFirstName(String firstName) {
 this.firstName = firstName;
 }

 public String getLastName() {
 return lastName;
 }

 public void setLastName(String lastName) {
 this.lastName = lastName;
 }

 public String getEmailId() {
 return emailId;
 }

 public void setEmailId(String emailId) {
 this.emailId = emailId;
 }

 @Override public String toString() {
 return "EmployeeDto{" +
 "id=" + id +
 ", firstName='" + firstName + '\'' +
 ", lastName='" + lastName + '\'' +
 ", emailId='" + emailId + '\'' +
 '}';
 }
 **/
}
