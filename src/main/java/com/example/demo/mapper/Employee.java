package com.example.demo.mapper;

import lombok.*;
import org.springframework.context.annotation.Bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@NoArgsConstructor
@Entity
@Data
@Table(name = "employees")

public class Employee  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private  int id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email_address")
    private String emailId;


 /**    public Employee() {
     }

     public int getId() {
     return id;
     }
     **/


 /*   public void setId(int id) {
        this.id = id;
    }

   public Employee(String firstName, String lastName, String emailId) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.emailId = emailId;
    }
**/
    /**
     public String getFirstName() {
     return firstName;
     }

     public void setFirstName(String firstName) {
     this.firstName = firstName;
     }

     public String getLastName() {
     return lastName;
     }

     public void setLastName(String lastName) {
     this.lastName = lastName;
     }

     public String getEmailId() {
     return emailId;
     }

     public void setEmailId(String emailId) {
     this.emailId = emailId;
     }
     **/
    /**
     @Override public String toString() {
     return "Employee{" +
     "id=" + id +
     ", firstName='" + firstName + '\'' +
     ", lastName='" + lastName + '\'' +
     ", emailId='" + emailId + '\'' +
     '}';
     }
     **/
}

