package com.example.demo.mapper;

import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;

public class MapperConfiguration extends ConfigurableMapper {
    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(Employee.class, EmployeeDto.class)
                .byDefault()
                .register();
    }
}
////////////////////////

