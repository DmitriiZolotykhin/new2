package com.example.demo.mapper;

import ma.glasnost.orika.BoundMapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.stereotype.Component;

@Component
public class MapperEmployeeToEmployeeDto {
    //todo добавь тут к названию Mapper и перенеси в каталог mapper
    //используй orika
    //не разобрался с orika

    public EmployeeDto convert(Employee employee) {

        EmployeeDto employeeDto = new EmployeeDto();
        employeeDto.setId(employee.getId());
        employeeDto.setFirstName(employee.getFirstName());
        employeeDto.setLastName(employee.getLastName());
        employeeDto.setEmailId(employee.getEmailId());

        return employeeDto;
    }
}
