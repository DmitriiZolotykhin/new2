package com.example.demo.mapper;


import com.example.demo.service.EmployeeService;
import ma.glasnost.orika.BoundMapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component

public class MapperEmployeeDtoToEmployee {
//todo добавь тут к названию Mapper и перенеси в каталог mapper---
//используй orika--
// не разобрался с orika


    private EmployeeService employeeService;

    @Autowired
    public void setEmployeeService(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    public Employee convert(EmployeeDto employeeDto) {

        Employee employee = (employeeDto.getId() != null ? employeeService.findById(employeeDto.getId()) : new Employee());


        employee.setFirstName(employeeDto.getFirstName());
        employee.setLastName(employeeDto.getLastName());
        employee.setEmailId(employeeDto.getEmailId());

        return employee;
    }
}


/**
 * public class MapperConfiguration extends ConfigurableMapper {
 *
 * @Override protected void configure(MapperFactory factory) {
 * factory.classMap(Employee.class, EmployeeDto.class)
 * .byDefault()
 * .register();
 * }
 * }
 **/