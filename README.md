Endpoints для Postman

http://localhost:8080/actuator/health актуатор

http://localhost:8080/app/employees get - показывает всех employee

http://localhost:8080/app/employees/104 get - показывает определенного employee

http://localhost:8080/app/employees/105 delete -удаляет определенного employee

http://localhost:8080/app/employees/101 put -обновляет определенного employee headers= Content-Type application/json

{ "firstName": "Ivandffdfgdgf7744444444444777777000000", "lastName": "Ivanov0000000000", "emailId": "qw@gmail.com0000000" }

http://localhost:8080/app/employees/ post - добавляет определенного employee headers= Content-Type application/json

{ "firstName": "Ivan", "lastName": "Ivanov", "emailId": "qw@gmail.com" }